Step 4: debugging performance issues
====================================

Context
^^^^^^^

We are now recording all the spikes of the network and displaying them in the brain visualizer. However, the shape that is highlighted does not give a lot of insight about what is happening in the network. For this, we need to provide a customized configuration file to the brain visualizer.


Custom brain visualizer
^^^^^^^^^^^^^^^^^^^^^^^

We are going to upload a customized configuration .json file for the brain visualizer that contains the position of each neuron of the robot's brain. The file is pre-written, but you could as well make your own script that reads the brain file and builds a different .json file.

The first thing to do is to stop the virtual experiment, because we want to update the setup file of the experiment (to make the reference to the .json file). Don't worry: the experiment saves everything you did up to now. To stop the experiment, click on the arrow button (the square one) on the top-left of the screen. Then click on "stop", and finally on "ok".

After stopping the experiment, click on our experiment ("Tutorial: Brain Visualizer"), and the on the "Files" button. You will be redirected to the list of all files that have been created to make the experiment work. The first thing we are going to do is to upload the .json configuration file. Click on the "upload file" button (top right of the list), and choose the file number 5 (5_neuron_positions.json), still in the materials folder.

The second thing we need to do is to make the reference to the custom configuration file, inside the setup file of the experiment. First, download this setup file, by hitting the "download" button of the line that goes after the file "experiment_configuration.exc". Then, go in your download folder and modify the file by adding the following line after the other line that begins with "<configuration ...".

.. code-block::html

    <configuration type="brainvisualizer" src="5_neuron_positions.json"/>

Then upload this modified file (so the one in your download folder) just as you did for the .json configuration file. When the dialog box appears, choose "yes", as it will replace the already existing .exc file. Then, you can go back to the experiment list by clicking on the tab "My experiments" and launch the tutorial experiment again. This might take a while this time (a few minutes), because the platform needs to recreate all the neurons and spike recorders.

After launching the experiment, hit the "play" button, open the brain visualizer again and move the "spike contrast" cursor to see the spikes. If everything went right, the "custom" option should be available in the brain visualizer and the neurons and their spikes should be displayed in a better way. It should look just as in the figure of step 2.

You can see the input brightness and darkness being first encoded in the LGN layers, then the vertical and horizontal features being detected in the different V1 and V2 layers and finally surface signals spreading in the different V4 layers (very fast). The spikes are updated as the stimuli change on the screen.

That's it! You now master the brain visualizer and you know how to customize it.
