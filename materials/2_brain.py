# ! /usr/bin/env python
# PyNN implementation of the LAMINART model of visual cortex.

from hbp_nrp_cle.brainsim import simulator as sim
import numpy as np
import nest
import logging
from std_msgs.msg import String
from specs import MyConnector, createConvFilters


########################################
### All parameters of the simulation ###
########################################


# Simulation parameters
resolution       =    1.0      # time step of the computation of the neurons dynamical equations
nThreadsToUse    =    7        # define here how many cores you want to use
logger           =    logging.getLogger(__name__)
sim.setup(timestep=resolution, min_delay=1.0, max_delay=2.0, threads=nThreadsToUse)

# Network parameters
imRows           =   30        # heigth of the Laminart model's input
imCols           =   60        # width  of the Laminart model's input
weightScale      =    0.001    # general weight for all connections between neurons !!!PyNN vs. NEST standard!!!
nSegLayers       =    1        # this is originally a network for segmentation, but there is no segmentation in this tutorial

# Convolution filter parameters (V1, V2)
nOri             =    2
convSize         =    6
phi              =    0
sigmaX           =    0.5
sigmaY           =    convSize
oLambda          =    4.0
thresh           =  250
V1_SurroundRange =    3

# Filling-in parameters (V4)
V4SpreadingRange =    1
nPols            =    2

# Neurons parameters (correspond to NEST default parameters)
cellParams = {
    'i_offset'   :   0.0,  # (nA)
    'tau_m'      :  10.0,  # (ms)
    'tau_syn_E'  :   2.0,  # (ms)
    'tau_syn_I'  :   2.0,  # (ms)
    'tau_refrac' :   2.0,  # (ms)
    'v_rest'     : -70.0,  # (mV)
    'v_reset'    : -70.0,  # (mV)
    'v_thresh'   : -55.0,  # (mV)
    'cm'         :   0.25} # (nF)
cellType = sim.IF_curr_alpha(**cellParams)

# Connection parameters
connections = {
    
    # LGN
    'LGN_ToV1Excite'      :   1500.0,

    # V1 and V2
    'V1_ComplexExcite'    :   1500.0,
    'V1_SurroundInhib'    :   -200.0,
    'V1_CrossOriInhib'    :  -2000.0,
    'V1_ToV2Excite'       :   5000.0,

    # V4 filling-in
    'V4_Competition'      :  -5000.0,
    'V4_Spreading'        :   1000.0,
    'V2_TriggerSpreading' :   1000.0,
    'V2_StopSpreading'    :   -400.0,

    # Reset signal (for blinks)
    'ResetInhib'          : -20000.0}

# Scale the weights, if needed
for key, value in connections.items():
    connections[key] = value*weightScale


################################################
### Input dimensions and orientation filters ###
################################################


# Read the image from Stimuli/ and create boundary coordinates (between pixels coordinates)
oriRows     = imRows + 1                              # Oriented contrasts map is in between regular pixels
oriCols     = imCols + 1                              # Oriented contrasts map is in between regular pixels
oppOri      = list(np.roll(range(nOri), nOri/2))      # Indexes for orthogonal orientations
convFilters = createConvFilters(nOri, size=convSize, phi=phi, sigmaX=sigmaX, sigmaY=sigmaY, oLambda=oLambda)


#################################################
### Create the neuron layers (LGN, V1, V2, V4 ###
#################################################


# LGN
LGN         = sim.Population(nPols*1*imRows*imCols, cellType, label='LGN')

# Areas V1/V2
V1Layer4    = sim.Population(nPols*nOri*oriRows*oriCols, cellType, label='V1Layer4')
V1Layer23   = sim.Population(1*nOri*oriRows*oriCols, cellType, label='V1Layer23')
V2Layer4    = sim.Population(1*nOri*oriRows*oriCols, cellType, label='V2Layer4')

# Area V4
V4          = sim.Population(nPols*1*imRows*imCols, cellType, label='V4')

# Reset cell (for sacadic inhibition and to reset segmentation)
someNeuron = sim.Population(1*1*1*1, cellType, label='someNeuron')


#######################################################################
###  Neurons layers are defined, now set up connexions between them ###
#######################################################################


############ V1 feedforward connections ############

for p in range(nPols):                                               # Polarities
    for k in range(nOri):                                            # Orientations
        for i2 in range(-convSize/2, convSize/2):                    # Filter rows
            for j2 in range(-convSize/2, convSize/2):                # Filter columns
                source  = []
                target  = []
                source2 = []
                target2 = []
                for i in range(convSize/2, oriRows-convSize/2):      # Rows
                    for j in range(convSize/2, oriCols-convSize/2):  # Columns
                        if i+i2 >=0 and i+i2<imRows and j+j2>=0 and j+j2<imCols:

                            # Connections from LGN to oriented polarized V1 cells, using convolutionnal filters
                            if abs(convFilters[p][k][i2+convSize/2][j2+convSize/2]) > 0.1:
                                source. append(   p *      imRows* imCols +                    (i+i2)*imCols  + (j+j2))
                                target. append(   p *nOri*oriRows*oriCols + k*oriRows*oriCols + i    *oriCols +  j    )
                                source2.append(   p *      imRows* imCols +                    (i+i2)*imCols  + (j+j2))
                                target2.append((1-p)*nOri*oriRows*oriCols + k*oriRows*oriCols + i    *oriCols +  j    )

                # Define the weight for this filter position and orientation
                polarWeight1 = connections['LGN_ToV1Excite']*convFilters[  p][k][i2+convSize/2][j2+convSize/2]
                polarWeight2 = connections['LGN_ToV1Excite']*convFilters[1-p][k][i2+convSize/2][j2+convSize/2]

                # LGN -> Layer 4 (simple cells) connections (no connections at the edges, to avoid edge-effects)
                sim.Projection(LGN, V1Layer4, MyConnector(source,  target ), sim.StaticSynapse(weight=polarWeight1))
                sim.Projection(LGN, V1Layer4, MyConnector(source2, target2), sim.StaticSynapse(weight=polarWeight2))

# Convergent excitatory connection from both polarities of V1Layer4 to V1Layer23 and then further to V2Layer4
source = []
target = []
for p in range(nPols):
    for k in range(nOri):
        for i in range(oriRows):
            for j in range(oriCols):

                source.append(p*nOri*oriRows*oriCols + k*oriRows*oriCols + i*oriCols + j)
                target.append(                         k*oriRows*oriCols + i*oriCols + j)

sim.Projection(V1Layer4,  V1Layer23, MyConnector(source, target), sim.StaticSynapse(weight=connections['V1_ComplexExcite']))
sim.Projection(V1Layer23, V2Layer4,  sim.OneToOneConnector(),     sim.StaticSynapse(weight=connections['V1_ToV2Excite'   ]))


############ V1/V2 competition stages ############

for k in range(nOri):                                 # Source orientation
    for k2 in range(nOri):                            # Target orientation

        # Close-range inhibition between similar orientation
        colinOriWeight = 1.0/(1.0 + 8.0/nOri*min((k-k2)%nOri, (k2-k)%nOri))  # how much colinear k and k2 are
        if colinOriWeight**2 > 0.1:
            VRange = V1_SurroundRange
            for i2 in range(-VRange, VRange+1):       # Filter rows
                for j2 in range(-VRange, VRange+1):   # Filter columns
                    if i2!=0 or j2!=0:

                        source  = []
                        source2 = []
                        target  = []
                        target2 = []
                        for i in range(oriRows):      # Rows
                            for j in range(oriCols):  # Columns
                                if 0 <= i+i2 < oriRows and 0 <= j+j2 < oriCols:

                                    source.append(k *oriRows*oriCols +  i    *oriCols +  j    )
                                    target.append(k2*oriRows*oriCols + (i+i2)*oriCols + (j+j2))

                        surroundWeight = colinOriWeight*connections['V1_SurroundInhib']/np.sqrt(i2**2+j2**2)
                        sim.Projection(V1Layer23, V2Layer4,  MyConnector(source, target), sim.StaticSynapse(weight=surroundWeight))

        # Local inhibition between different orientations
        orthoOriWeight = 1.0/(1.0 + 8.0/nOri*min((k-oppOri[k2])%nOri, (oppOri[k2]-k)%nOri))  # how much orthogonal k and k2 are
        if orthoOriWeight**2 > 0.1:

            source  = []
            target  = []
            for i in range(oriRows):      # Rows
                for j in range(oriCols):  # Columns

                        source.append(k *oriRows*oriCols + i*oriCols + j)
                        target.append(k2*oriRows*oriCols + i*oriCols + j)

            crossOriWeight = orthoOriWeight*connections['V1_CrossOriInhib']
            sim.Projection(V1Layer23, V1Layer23, MyConnector(source, target), sim.StaticSynapse(weight=crossOriWeight))


############## Area V4 ################

source  = []
source2 = []
source3 = []
source4 = []
source5 = []
target  = []
target2 = []
target3 = []
target4 = []
target5 = []
for i in range(imRows):
    for j in range(imCols):

        # V4 signals spread in any direction
        for i2 in [-V4SpreadingRange, V4SpreadingRange]:       # [-V4SpreadingRange, 0, V4SpreadingRange]
            for j2 in [-V4SpreadingRange, V4SpreadingRange]:   # [-V4SpreadingRange, 0, V4SpreadingRange]
                if i+i2 >=0 and i+i2<imRows and j+j2>=0 and j+j2<imCols:
                    for p in range(nPols):                     # Polarities
                    
                        # Spread in the normal V4 layer
                        source.append(p*imRows*imCols + i    *imCols +  j    )
                        target.append(p*imRows*imCols +(i+i2)*imCols + (j+j2))

        # V2 cells inhibit filling-in and V1 cells trigger filling-in in specific directions
        for k in range(nOri):                                  # Orientations
            filterSize = convFilters[0][k].shape[0]            # Size of the filter to span
            for i2 in range(-filterSize/2, filterSize/2):      # Filter rows
                for j2 in range(-filterSize/2, filterSize/2):  # Filter columns
                    if i+i2 >=0 and i+i2<imRows and j+j2>=0 and j+j2<imCols:
                        for p in range(nPols):                 # Polarities

                            # Connection from V2Layer23 to V4 cells, on both sides
                            if abs(convFilters[0][k][i2+filterSize/2][j2+filterSize/2]) > 0.1:  # any([:]? more elegant)
                                source2.append(k*oriRows*oriCols +  i    *oriCols +  j    )
                                target2.append(p* imRows* imCols + (i+i2)* imCols + (j+j2))

                            # Connection from V1Layer4 to V4 cells, in the orthogonal direction as the V1 orientation (1st side)
                            if convFilters[1][k][i2+filterSize/2][j2+filterSize/2] > 0.1:
                                source3.append(   p *nOri*oriRows*oriCols + k*oriRows*oriCols +  i    *oriCols +  j    )
                                target3.append(   p *      imRows* imCols +                     (i+i2)* imCols + (j+j2))
                        
                            # Connection from V1Layer4 to V4 cells, in the orthogonal direction as the V1 orientation (2nd side)
                            if convFilters[0][k][i2+filterSize/2][j2+filterSize/2] > 0.1:
                                source4.append(   p *nOri*oriRows*oriCols + k*oriRows*oriCols +  i    *oriCols +  j    )
                                target4.append((1-p)*      imRows* imCols +                     (i+i2)* imCols + (j+j2))

        # V4 polarities compete (bright vs dark)
        for p in range(nPols):
            source5.append(   p *imRows*imCols + i*imCols + j)
            target5.append((1-p)*imRows*imCols + i*imCols + j)

# V4 activity spreads in any direction
sim.Projection(V4,       V4, MyConnector(source, target),   sim.StaticSynapse(weight=connections['V4_Spreading'       ]))

# V2 non-polar activity stops spreading, so that filling-in stops at the shapes boundaries
sim.Projection(V2Layer4, V4, MyConnector(source2, target2), sim.StaticSynapse(weight=connections['V2_StopSpreading'   ]))

# V2 polar activity triggers spreading directionnally, so that the shape brightness is on the correct side
sim.Projection(V1Layer4, V4, MyConnector(source3, target3), sim.StaticSynapse(weight=connections['V2_TriggerSpreading']))
sim.Projection(V1Layer4, V4, MyConnector(source4, target4), sim.StaticSynapse(weight=connections['V2_TriggerSpreading']))

# V4 brightness and darkness signals compete between each other
sim.Projection(V4,       V4, MyConnector(source5, target5), sim.StaticSynapse(weight=connections['V4_Competition'     ]))


############## Reset ################

sim.Projection(someNeuron, LGN, sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
sim.Projection(someNeuron, V4,  sim.AllToAllConnector(), sim.StaticSynapse(weight=connections['ResetInhib']))
